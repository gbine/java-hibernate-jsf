package br.edu.guairaca.hibernate.dao;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import br.edu.guairaca.hibernate.model.Estado;

@Transactional
public class EstadoDAO extends GenericDAO<Estado, Long> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected EstadoDAO(EntityManager entityManager) {
		super(entityManager);
	}

	@SuppressWarnings("unchecked")
	public List<Estado> buscarPorNomeESigla(String nome, String sigla) {
		if (nome != null && nome.isEmpty()) {
			nome = null;
		}

		if (sigla != null && sigla.isEmpty()) {
			sigla = null;
		}

		StringBuffer hql = new StringBuffer();
		hql.append("select e from Estado e ");
		hql.append(" where e.sigla = coalesce(:sigla, e.sigla) ");
		hql.append("   and e.nome  = coalesce(:nome, e.nome)");

		Query query = entityManager.createQuery(hql.toString());
		query.setParameter("sigla", sigla);
		query.setParameter("nome", nome);

		return query.getResultList();
	}
}
